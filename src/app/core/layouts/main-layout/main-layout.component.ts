import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor() { }
  icons = [
    ' fa-figma',
    'fa-html5',
    'fa-css3',
    'fa-square-js',
    'fa-angular',
    'fa-vuejs',
  ]

  ngOnInit(): void {
  }

}

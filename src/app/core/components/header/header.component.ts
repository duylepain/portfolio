import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ContactMeComponent } from '../contact-me/contact-me.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  bsModalRef?: BsModalRef;
  constructor(
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
  }


  contactMe(){
    const initialState: ModalOptions = {
      class: 'flex items-center h-screen',
      initialState: {
        list: [
          'Open a modal with component',
          'Pass your data',
          'Do something else',
          '...'
        ],
        title: 'Modal with component'
      }
    };
    this.bsModalRef = this.modalService.show(ContactMeComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { ContactMeComponent } from './contact-me/contact-me.component';
import { IntroComponent } from './intro/intro.component';
import { ExpComponent } from './exp/exp.component';



@NgModule({
  declarations: [
    HeaderComponent,
    ContactMeComponent,
    IntroComponent,
    ExpComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    HeaderComponent,
    ContactMeComponent,
    IntroComponent,
    ExpComponent


  ]
})
export class ComponentsModule { }

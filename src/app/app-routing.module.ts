import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path:'profile',
    loadChildren: ()=>import('./../app/core/layouts/main-layout/main-layout.module').then(m=>m.MainLayoutModule)
  },
  {
    path: '',
    redirectTo: 'profile', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../core/components/components.module';
import { ModalModule } from 'ngx-bootstrap/modal';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ComponentsModule,




    // ngx-bootstrap
    ModalModule.forRoot(),
  ],
  exports: [
    ComponentsModule
  ]
})
export class SharedModule { }

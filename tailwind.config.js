/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        'blue': '#0A0119',
        'violet': '#8758FF',
        'violet-base': '#2D3540',
      },
      fontSize: {
        'base': '18px'
      }
    },
  },
  plugins: [],
}
